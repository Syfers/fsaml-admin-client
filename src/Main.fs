module Main

open Feliz
open App
open Browser.Dom
open Fable.Core.JsInterop
open Fable.Core
open Browser.Types

importSideEffects "./styles/global.scss"

[<Emit("AOS.init()")>]
let private initAOS() = jsNative
[<Emit("Array.from($0)")>]
let private ofArray(_: NodeListOf<'a>): 'a array = jsNative

initAOS();
document.addEventListener("DOMContentLoaded",
                          fun _ ->
                            let burgers = document.querySelectorAll(".navbar-burger") |> ofArray in
                            Array.iter (
                                fun (burger: Element) ->
                                    do burger.addEventListener("click",
                                                            fun _ ->
                                                                let targetElement =
                                                                    burger.getAttribute "data-target"
                                                                    |> document.getElementById
                                                                in
                                                                do targetElement.classList.toggle("is-active") |> ignore
                                                        )

                            ) burgers
                          )

ReactDOM.render(
    Components.Router(),
    document.getElementById "feliz-app"
)
