module ModalForm

open Fable
open FSharpPlus.Operators
open FSharpPlus.Data
open Feliz
open Thoth.Json
open Thoth.Fetch
open System
open Fetch.Types
open Fable.Core

type Errors = Map<string, string>

type PostingStatus =
    | Posting
    | Posted
    | ToBePosted

type FormContext =
    {
        formValue: obj
        setFormValue: obj
        errors: Errors
        postingStatus: PostingStatus
        issueSearch: unit -> unit
    }

let formContext = React.createContext("formContext")

type ModalFormInputProps<'form> =
    {
        _postUrl: string
        _validator: Option<'form -> State<Errors, 'form>>
        _formRenderer: ReactElement
        _packageEncoder: 'form -> JsonValue
        _defaultValue: 'form option
        _method: HttpMethod
    }

type private ModalFormProps<'form> =
    {
        postUrl: string
        validator: 'form -> State<Errors, 'form>
        formRenderer: ReactElement
        defaultValue: 'form
        packageEncoder: 'form -> JsonValue
        method: HttpMethod
    }

module ModalFormProps =
    let url url =
        State.modify <| fun props -> { props with _postUrl = url }
    let httpMethod method =
        State.modify <| fun props -> { props with _method = method }
    let renderer renderer =
        State.modify <| fun props -> { props with _formRenderer = renderer }
    let rule propName validator form =
            State
            <| fun errors ->
                match validator form with
                | Ok form' -> form', errors
                | Error m -> form, (Map.add propName m errors)
    let rules ruleList =
        State.modify <| fun props -> { props with _validator = Some <| List.reduce (>=>) ruleList }
    let initialValue value =
        State.modify <| fun props -> { props with _defaultValue = Some value }
    let encoder encoder =
        State.modify <| fun props -> { props with  _packageEncoder = encoder }

module Helpers =
    let disabableProps flag props = if flag then Css.IsDisabled :: props else props
    let errorProps flag props = if flag then Css.IsDanger :: props else props
    let hasErrors key errors = Map.containsKey key errors
    let isPosting postingStatus = Posting = postingStatus

[<ReactComponent>]
let ModalDecorator (title: string) (submitText: string) child =
    let close = React.useContext(StreamIndexComponents.modalContext)
    let { postingStatus = postingStatus
          errors = errors
          issueSearch = issueSearch
        } = React.useContext(formContext)
    let disabableProps flag props = if flag then Css.IsDisabled :: props else props
    let errorProps flag props = if flag then Css.IsDanger :: props else props
    let hasErrors key = Map.containsKey key errors
    let isPosting = Posting = postingStatus
    React.useEffect(
        (fun () ->
            if postingStatus = Posted then
                close()
        ),
        [| box postingStatus |]
    )
    Html.div [
        prop.className [ Css.ModalCard; Css.P2 ]
        prop.children [
            Html.header [
                prop.className [ Css.ModalCardHead; Css.HasBackgroundDark ]
                prop.children [
                    Html.p [ prop.className [ Css.ModalCardTitle; Css.HasTextWhite ]
                             prop.text title ]
                    Html.button [
                        prop.className Css.Delete
                        prop.onClick (fun _ -> if postingStatus <> Posting then close())
                    ]
                ]
            ]
            Html.section [
                prop.className Css.ModalCardBody
                prop.children [
                    if Map.containsKey "posting" errors then
                        yield Html.article [
                            prop.className [ Css.Message; Css.IsDanger ]
                            prop.children [
                                Html.div [
                                    prop.className Css.MessageBody
                                    prop.text errors.["posting"]
                                ]
                            ]
                        ]
                    yield child
                ]
            ]
            Html.footer [
                prop.className [ Css.ModalCardFoot; Css.HasBackgroundWhite ]
                prop.children [
                    Html.button [
                        prop.className [ Css.Button; "fbutton"; if postingStatus = Posting then Css.IsLoading; ]
                        prop.onClick (fun _ -> issueSearch())
                        prop.disabled isPosting
                        prop.text submitText
                    ]
                    Html.button [
                        prop.className Css.Button
                        prop.onClick (fun _ -> if postingStatus <> Posting then close())
                        prop.text "Cancelar"
                    ]
                ]
            ]
        ]
    ]

[<ReactComponent>]
let ModalForm propList =
    let propInputs =
        List.fold
        <| (flip State.exec)
        <| {
            _postUrl = null
            _validator = None
            _formRenderer = Html.none
            _defaultValue = None
            _packageEncoder = fun _ -> Encode.nil
            _method = HttpMethod.POST
        }
        <| propList
    let props = {
        postUrl = propInputs._postUrl
        validator = Option.get propInputs._validator
        formRenderer = propInputs._formRenderer
        defaultValue = Option.get propInputs._defaultValue
        packageEncoder = propInputs._packageEncoder
        method = propInputs._method
    }

    let (formValue, setFormValue) = React.useState(fun () -> props.defaultValue)
    let (postingStatus, setPostingStatus) = React.useState(ToBePosted)
    let (errors, setErrors) = React.useState(Map.empty)
    let validate continuation =
        let actualErrors = State.exec (props.validator formValue) Map.empty
        if Map.isEmpty actualErrors  then
            continuation()
        else
            do setErrors actualErrors
    let issueSearch =
        React.useCallback(
            (fun () ->
                validate
                <| fun () ->
                        async {
                           do setPostingStatus Posting
                           let! responseResult =
                                if props.method = HttpMethod.POST then
                                    Fetch.tryPost(props.postUrl,
                                                  props.packageEncoder formValue,
                                                  properties = [
                                                    RequestProperties.Mode RequestMode.Cors
                                                  ])
                                else
                                    Fetch.tryPut(props.postUrl,
                                                 props.packageEncoder formValue,
                                                 properties = [
                                                    RequestProperties.Mode RequestMode.Cors
                                                 ])
                                |> Async.AwaitPromise
                           do Console.WriteLine(responseResult)
                           match responseResult with
                           | Ok response ->
                               do Console.WriteLine(response :> obj)
                               do setPostingStatus Posted
                           | Error (FetchFailed r) ->
                               do setErrors
                               << Map.ofList
                               <| [ "posting",
                                    sprintf "No se pudo registrar los datos del paciente %s"
                                    <| r.StatusText ]
                               do setPostingStatus ToBePosted
                            | Error (NetworkError e) ->
                               do setErrors
                               << Map.ofList
                               <| [ "posting",
                                    sprintf "No se pudo conectar con el servidor %s"
                                    <| e.Message ]
                               do setPostingStatus ToBePosted
                            | Error (PreparingRequestFailed e) ->
                               do setErrors
                               << Map.ofList
                               <| [ "posting",
                                    sprintf """No se pudo crear la petición para
                                            enviar al servidor: %s"""
                                    <| e.Message ]
                               do setPostingStatus ToBePosted
                            | Error (DecodingFailed e) ->
                               do setErrors
                               << Map.ofList
                               <| [ "posting",
                                    sprintf """Respuesta recibida no entendida %s""" e ]
                               do setPostingStatus ToBePosted
                        } |> Async.Start
             ),
            [| box formValue; box Errors |]
        )
    React.useEffect(
        (fun () ->
            validate (fun () -> do setErrors Map.empty )
         ),
        [| box formValue |]
    )
    let context = {
        postingStatus = postingStatus
        formValue = formValue :> obj
        setFormValue = setFormValue :> obj
        errors = errors
        issueSearch = issueSearch
    }
    React.contextProvider(formContext, context,
                          props.formRenderer)
