namespace App

open Layout
open Feliz
open Feliz.Router
open FSharpPlus.Operators

type Components =
    /// <summary>
    /// The simplest possible React component.
    /// Shows a header with the text Hello World
    /// </summary>
    [<ReactComponent>]
    static member HelloWorld() = Html.h1 "Hello World"

    /// <summary>
    /// A stateful React component that maintains a counter
    /// </summary>
    [<ReactComponent>]
    static member Counter() =
        let (count, setCount) = React.useState(0)
        Html.div [
            Html.h1 count
            Html.button [
                prop.onClick (fun _ -> setCount(count + 1))
                prop.text "Increment"
            ]
        ]
    /// <summary>
    /// A React component that uses Feliz.Router
    /// to determine what to show based on the current URL
    /// </summary>
    [<ReactComponent>]
    static member Router() =
        let (currentUrl, updateUrl) = React.useState(Router.currentUrl())
        let inUrl = (flip List.contains) currentUrl
        React.router [
            router.onUrlChanged updateUrl
            router.children ([
                match currentUrl with
                | [ "pacientes" ] -> [ PatientManagement.PatientIndex() ] |> OneColumnLayout
                | [ "programado" ] ->
                    [ ConsultationManagement.ConsultationIndex() ]
                    |> OneColumnLayout
                | [ "atpacientes" ] -> [ Components.Counter() ] |> OneColumnLayout
                | _ -> Html.h1 "Not found"
            ]
            |> MainLayout
                [ { name = "Pacientes"; link = "#/pacientes"; selected = inUrl "pacientes" ; icon = Icons.FaUser }
                  { name = "Visitas y Citas"; link = "#/programado"; selected = inUrl "programado" ; icon = Icons.FaCalendar }
                  { name = "At. al Paciente"; link = "#/atpacientes"; selected = inUrl "atpacientes" ; icon = Icons.FaHospitalO }
                ])
        ]
