module StreamProtocol

open Fable
open Feliz
open FSharpPlus
open Thoth.Json
open FSharpPlus.Data
open Fable.React
open Fable.Core
open Browser
open Browser.Types
open System
open Microsoft.FSharp.Control

type SearchState<'pkg> =
    | NoResult of string
    | Result of 'pkg list
    | Searching of SearchState<'pkg>
    | SearchError of string * SearchState<'pkg>

    member this.IsResultOrSearching =
        match this with
            | Result _ | Searching _ -> true
            | _ -> false

    member this.InSearch =
        match this with
            | Searching _ -> true
            | _ -> false

type Query<'queryType> = {
    ``type``: 'queryType
    search: string
    pageSearch: int
}

type QueryResult<'pkg> = {
    queryId: int
    package: 'pkg
}

type QueryResultInfo = {
    queryId: int
    pages: int
}


type StreamProtocolInputOptions<'pkg, 'queryType> = {
    _wsUrl: string
    _packageDecoder: Decoder<'pkg> option
    _queryEncoder: Option<Query<'queryType> -> int -> JsonValue>
    _children: ReactElement list
    _initialDescriptor: 'queryType option
    _storeName: string
    _descriptorToDisplay: Option<'queryType -> string>
    _displayToDescriptor: Option<string -> 'queryType>
    _packageEncoder: 'pkg -> JsonValue
}

type StreamProtocolOptions<'pkg, 'queryType> = {
    wsUrl: string
    packageDecoder: Decoder<'pkg>
    queryEncoder: Query<'queryType> -> int -> JsonValue
    children: ReactElement list
    storeName: string
    initialDescriptor: 'queryType
    descriptorToDisplay: 'queryType -> string
    displayToDescriptor: string -> 'queryType
    packageEncoder: 'pkg -> JsonValue
}

module StreamProtocolProps =
    let storeName name =
        State.modify
        <| fun options ->
            { options with _storeName = name }
    let url wsUrl =
        State.modify
        <| fun options ->
            { options with _wsUrl = wsUrl }
    let packageEncoder encoder =
        State.modify
        <| fun options ->
            { options with _packageEncoder = encoder }
    let packageDecoder decoder =
        State.modify
        <| fun options ->
            { options with _packageDecoder = Some decoder }
    let queryEncoder encoder =
        State.modify
        <| fun options ->
            { options with _queryEncoder = Some encoder }
    let children children =
        State.modify
        <| fun options ->
            { options with _children = children }
    let initialDescriptor desc =
        State.modify
        <| fun options ->
            { options with _initialDescriptor = Some desc }
    let descriptorToDisplay dToDisplay =
        State.modify
        <| fun options ->
            { options with _descriptorToDisplay = Some dToDisplay }
    let displayToDescriptor dToDescriptor =
        State.modify
        <| fun options ->
            { options with _displayToDescriptor = Some dToDescriptor }

let private connectingMessage = """
    Conectando con el servidor...
"""
let private emptySearchMessage = """
    ¡Haga una búsqueda para obtener resultados!
"""
let private disconnectedErrorMessage = """
    Se ha perdido la conexión con el servidor... ¡Recargue la página!
"""

type ProtocolState<'pkg, 'queryType> = {
    searchState: SearchState<'pkg>
    query: Query<'queryType>
    currentPages: int
}

type ProtocolContext = {
    protocolState: obj
    setQuery: obj
}

type WebSocketContext = {
    ws: Lazy<WebSocket>
    setUrl: string -> unit
}

let wsProtocolContext = React.createContext("protocolContext")
let webSocketContext = React.createContext("wsContext")

let private packageDecoder decoder =
    Decode.oneOf [
        Decode.object
            (fun get ->
                {
                    queryId = get.Required.Field "query_id" Decode.int
                    pages = get.Required.Field "total_pages" Decode.int
                }
             )
        |> Decode.map (Choice2Of3)
        Decode.object (fun get -> get.Required.Field "msg" Decode.string)
        |> Decode.map (Choice3Of3)
        decoder
        |> Decode.map (Choice1Of3)
    ]

[<Emit("AOS.refreshHard()")>]
let private refreshAOS _ = jsNative

let private decodeStorage pkgDcd =
    Decode.list pkgDcd

[<ReactComponent>]
let StreamProtocolProvider options =
    let queryEncoder pkg = Encode.object [
        "search", Encode.string pkg.search
        "pageSearch", Encode.int pkg.pageSearch
        "type", Encode.string <| options.descriptorToDisplay pkg.``type``
    ]
    refreshAOS()
    let hasSavedItems =
        let item = localStorage.getItem(options.storeName)
        item <> null
        && item <> "null"
        && item <> "undefined"
    let (protocolState, setProtocolState) = React.useState({
        searchState =
            if not hasSavedItems then
                NoResult connectingMessage
            else
                Searching <| Result [  ]
        query =
                let decoder =
                    Decode.object (
                        (fun get -> {
                            search = get.Required.Field "search" Decode.string
                            pageSearch = get.Required.Field "pageSearch" Decode.int
                            ``type`` = options.displayToDescriptor
                                            <| get.Required.Field "type" Decode.string
                        })
                    )
                try
                    Decode.fromString decoder (localStorage.getItem(options.storeName))
                    |> Result.get
                with
                | e ->
                    do Console.Error.WriteLine(e)
                    { ``type`` = options.initialDescriptor
                      search = ""
                      pageSearch = 1 }
        currentPages = 1
    })
    let updateQuery =
        React.useCallback(
            (fun query -> setProtocolState { protocolState with query = query }),
            [| box protocolState; box setProtocolState |]
        )
    let queryId = React.useRef(0)
    let query = protocolState.query
    let ws: WebSocket = React.useContext(webSocketContext)
    ws.onclose <- fun e ->
        do Console.WriteLine(e)
        setProtocolState
            { protocolState with
                searchState =
                    SearchError (sprintf "%s: %s Código (%d)"
                                 <| disconnectedErrorMessage
                                 <| e.reason
                                 <| e.code,
                        protocolState.searchState) }
    ws.onopen <- fun _ ->
        setProtocolState { protocolState with query = { query with search = query.search } }
    ws.onmessage <- fun e ->
        if query.search <> "" then
            let decodeResult =
                Decode.fromString (
                    packageDecoder(options.packageDecoder)
                ) <| e.data.ToString()
                |> Result.bind ( function
                                 | Choice1Of3 package -> Ok << Choice1Of3 <| package
                                 | Choice2Of3 info when info.queryId = queryId.current ->
                                    Ok << Choice2Of3 <| info
                                 | Choice3Of3 "REFRESH" -> Ok <| Choice3Of3 "REFRESH"
                                 | _ -> Error "Orphan package")
            match decodeResult with
                | Ok(Choice2Of3 info) when info.pages >= 1 || info.pages = -1 ->
                    setProtocolState { protocolState with searchState = Result [  ]; currentPages = info.pages }
                | Ok(Choice2Of3 info) when info.pages = 0 ->
                    setProtocolState { protocolState
                                       with searchState = NoResult (
                                                            sprintf
                                                            <| """La búsqueda con parámetro "%s" para el criterio "%s"
                                                            no dieron resultados"""
                                                            <| query.search
                                                            <| (options.descriptorToDisplay query.``type``) ) }
                | Ok(Choice1Of3 package) ->
                    match protocolState.searchState with
                    | Result previousResult ->
                        setProtocolState
                            { protocolState with
                                searchState = Result <| previousResult @ [ package ] }
                    | _ ->
                        setProtocolState
                            { protocolState with searchState = Result <| [ package ] }
                | Ok(Choice3Of3 "REFRESH") ->
                    setProtocolState { protocolState with searchState = Result [  ] }
                | _ -> ()
    React.useEffect(
        ( fun () ->
            match ws.readyState with
            | WebSocketState.OPEN when query.search <> "" ->
                do queryId.current <- queryId.current + 1
                do localStorage.setItem(options.storeName, Encode.toString 1 (queryEncoder query))
                do ws.send <| Encode.toString 1 (options.queryEncoder query queryId.current)
                do setProtocolState
                    { protocolState with searchState = Searching protocolState.searchState }
            | WebSocketState.OPEN when query.search = "" ->
                do localStorage.setItem(options.storeName, Encode.toString 1 Encode.nil)
                do setProtocolState
                    { protocolState with searchState = NoResult emptySearchMessage }
            | WebSocketState.CLOSED ->
                match protocolState.searchState with
                | SearchError(_, _) -> ()
                | state ->
                    do setProtocolState
                        { protocolState with
                            searchState = SearchError (disconnectedErrorMessage, state) }
            | WebSocketState.CONNECTING when not hasSavedItems ->
                do setProtocolState
                    { protocolState with searchState = NoResult emptySearchMessage }
            | _ ->
                ()
          )
        , [| box query |]
    )
    React.contextProvider(
        wsProtocolContext,
        {
            protocolState = protocolState :> obj
            setQuery = updateQuery :> obj
        },
        React.fragment(Seq.ofList options.children)
    )

[<ReactComponent>]
let WebSocketProvider url children =
    let ws = WebSocket.Create(url)
    React.useEffectOnce(
        (fun () -> {
            new IDisposable with member _.Dispose() = ws.close()
        }))
    React.contextProvider(
        webSocketContext,
        ws,
        React.fragment(Seq.ofList children)
    )

[<ReactComponent>]
let StreamProtocol props =
    let inputOptions =
        List.fold
        <| (flip State.exec)
        <| {
            _wsUrl = null
            _packageDecoder = None
            _queryEncoder = None
            _initialDescriptor = None
            _children = [  ]
            _descriptorToDisplay = None
            _storeName = ""
            _packageEncoder = konst Encode.nil
            _displayToDescriptor = None
        }
        <| props
    let options = {
        wsUrl = inputOptions._wsUrl
        packageDecoder = Option.get inputOptions._packageDecoder
        queryEncoder = Option.get inputOptions._queryEncoder
        initialDescriptor = Option.get inputOptions._initialDescriptor
        children = inputOptions._children
        descriptorToDisplay = Option.get inputOptions._descriptorToDisplay
        displayToDescriptor = Option.get inputOptions._displayToDescriptor
        storeName = inputOptions._storeName
        packageEncoder = inputOptions._packageEncoder
    }
    StreamProtocolProvider(options)
