module ConsultationManagement
open Feliz
open Thoth.Json
open Thoth.Fetch
open Fable.Core
open FSharpPlus
open StreamProtocol
open StreamIndex
open StreamIndexComponents
open ModalForm
open System
open Browser.Types
open Microsoft.FSharp.Core
open PatientManagement

type ConsultationStatus =
    Cancelled
    | InProcess
    | Accepted

    override this.ToString() =
        match this with
        | Cancelled -> "Cancelada"
        | InProcess -> "En proceso"
        | Accepted -> "Aceptada"

type private ServiceData = {
    inactive: bool
    name: string
    specialistFullName: string
    price: float
}

type private ConsultationData = {
    patientCid: int
    consultationDate: DateTime
    status: ConsultationStatus
    reason: string
    id: int
}

type private PatientInfo = {
    personalInfo: UserRecord option
    historyInfo: History list option
}

type ConsultationDescriptors =
    Cid
    | ConsultationDate
    | Reason
    | Status
    | All

let private serviceDecoder: Decoder<ServiceData> =
    Decode.object
    <| fun get ->
        {
            name = get.Required.Field "name" Decode.string
            price = get.Required.Field "price" Decode.float
            inactive = None <> get.Optional.Field "inactive_date" Decode.datetime
            specialistFullName =
                sprintf "%s, %s"
                <| get.Required.Field "names" Decode.string
                <| get.Required.Field "lastnames" Decode.string
        }

let private mustBeTodayOrPosterior consultation =
    if consultation.consultationDate >= DateTime.Today then
        Ok consultation
    else
        Error "Fecha de la visita o cita debe ser mayor que la fecha actual"

let private mustNotBeEmpty field empty msg consultation =
    if field consultation = empty then
        Error msg
    else
        Ok consultation

let private encodeConsultation consultation =
    Encode.object [
        "patient_id", Encode.int consultation.patientCid
        "consultation_date", Encode.datetime consultation.consultationDate
        "status", Encode.int (
            match consultation.status with
            | Cancelled -> 1
            | InProcess -> 2
            | Accepted -> 3
        )
        "reason", Encode.string consultation.reason
    ]

let private encodeConsultationStringDate consultation =
    Encode.object [
        "patient_id", Encode.int consultation.patientCid
        "consultation_date",
            consultation.consultationDate.ToString("yyyy-MM-dd")
            |> Encode.string
        "status", Encode.int (
            match consultation.status with
            | Cancelled -> 1
            | InProcess -> 2
            | Accepted -> 3
        )
        "reason", Encode.string consultation.reason
    ]

let private updateRequest consultation =
    Encode.object [
        "pid", Encode.int consultation.id
        "consultation", encodeConsultationStringDate consultation
    ]

let private encodePackage query queryId =
    Encode.object [
        "type", Encode.string <| query.``type``.ToString()
        "operation",
            match query.``type`` with
            | Cid | All | Reason -> Encode.string "is_like"
            | Status -> Encode.string "is_status_like"
            | ConsultationDate -> Encode.string "is_date_before"
        "search",
            (
                let date =
                    try
                        Some <| DateTime.Parse(query.search)
                    with
                    | _ -> None
                match date with
                | Some date when query.``type`` = ConsultationDate
                    -> Encode.int64 <| ((DateTimeOffset) (date)).ToUnixTimeSeconds()
                | None when query.``type`` = ConsultationDate -> Encode.string ""
                | _ -> Encode.string query.search
            )
        "query_id", Encode.int queryId
        "page", Encode.int query.pageSearch
    ]

[<ReactComponent>]
let private ConsultationForm() =
    let close = React.useContext(modalContext)
    let {
            postingStatus = postingStatus
            errors = errors
            issueSearch = issueSearch
            formValue = formValue
            setFormValue = setFormValue
        } = React.useContext(formContext)
    let consultation = downcast formValue
    let setConsultation = downcast setFormValue
    let disabableProps = Helpers.disabableProps
    let errorProps = Helpers.errorProps
    let hasErrors key = Helpers.hasErrors key errors
    let isPosting = Helpers.isPosting postingStatus
    React.fragment [
        Html.div [
            prop.className [ Css.Field; Css.IsHorizontal ]
            prop.children [
                Html.div [
                    prop.className Css.FieldBody
                    prop.children [
                        Html.div [
                            prop.className Css.Field
                            prop.children [
                                Html.div [
                                    prop.className [ Css.Control; Css.HasIconsLeft ]
                                    prop.children [
                                        Html.input [
                                            prop.className (
                                                [ Css.Input ]
                                                |> errorProps (hasErrors <|
                                                               nameof consultation.patientCid)
                                            )
                                            prop.disabled isPosting
                                            prop.type' "number"
                                            prop.placeholder "Cédula del Paciente"
                                            prop.value (
                                                if consultation.patientCid = 0 then
                                                    ""
                                                else
                                                    consultation.patientCid.ToString()
                                            )
                                            (prop.onChange: (string -> unit) -> _)
                                            <| (fun cid ->
                                                setConsultation
                                                <| { consultation with
                                                        patientCid =
                                                            (if cid = "" then 0 else Int32.Parse cid) })
                                        ]
                                        Html.span [
                                            prop.className [ Css.Icon; Css.IsLeft ]
                                            prop.children [
                                                Html.i [
                                                    prop.className [
                                                        Css.Fa
                                                        Icons.FaUser
                                                    ]
                                                ]
                                            ]
                                        ]
                                        if hasErrors <| nameof consultation.patientCid then
                                            Html. p [
                                                prop.className [ Css.Help; Css.IsDanger ]
                                                prop.text errors.[nameof consultation.patientCid]
                                            ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
        Html.div [
            prop.className [ Css.Field; Css.IsHorizontal ]
            prop.children [
                Html.div [
                    prop.className Css.FieldBody
                    prop.children [
                        Html.div [
                            prop.className Css.Field
                            prop.children [
                                Html.div [
                                    prop.className [ Css.Control; Css.HasIconsLeft ]
                                    prop.children [
                                        Html.input [
                                            prop.className (
                                                [ Css.Input ]
                                                |> errorProps (hasErrors <|
                                                               nameof consultation.consultationDate)
                                            )
                                            prop.placeholder "Fecha de la visita/cita"
                                            prop.onFocus(fun e ->
                                                let el: HTMLInputElement = downcast e.target
                                                el.``type`` <- "date"
                                            )
                                            prop.disabled isPosting
                                            prop.value (
                                                if consultation.consultationDate = DateTime(0,0,0) then
                                                    ""
                                                else
                                                    consultation.consultationDate.ToString("yyyy-MM-dd")
                                            )
                                            (prop.onChange: ( DateTime -> _ ) -> _ )
                                            <| (fun date ->
                                                setConsultation
                                                <| { consultation with
                                                        consultationDate = date })
                                        ]
                                        Html.span [
                                            prop.className [ Css.Icon; Css.IsLeft ]
                                            prop.children [
                                                Html.i [
                                                    prop.className [
                                                        Css.Fa
                                                        Icons.FaCalendar
                                                    ]
                                                ]
                                            ]
                                        ]
                                        if hasErrors <| nameof consultation.consultationDate then
                                            Html. p [
                                                prop.className [ Css.Help; Css.IsDanger ]
                                                prop.text errors.[nameof consultation.consultationDate]
                                            ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
        Html.div [
            prop.className [ Css.Field; Css.IsHorizontal ]
            prop.children [
                Html.div [
                    prop.className Css.FieldBody
                    prop.children [
                        Html.div [
                            prop.className Css.Field
                            prop.children [
                                Html.div [
                                    prop.className [ Css.Control ]
                                    prop.children [
                                        Html.textarea [
                                            prop.className (
                                                [ Css.Textarea ]
                                                |> disabableProps (postingStatus = Posting)
                                                |> errorProps (hasErrors <|
                                                               nameof consultation.reason)
                                            )
                                            prop.disabled isPosting
                                            prop.placeholder "Razón de la visita..."
                                            prop.value consultation.reason
                                            (prop.onChange: (string -> unit) -> _)
                                            <| (fun reason ->
                                                setConsultation { consultation with
                                                                    reason = reason })
                                        ]
                                        if hasErrors <| nameof consultation.reason then
                                            Html. p [
                                                prop.className [ Css.Help; Css.IsDanger ]
                                                prop.text errors.[nameof consultation.reason]
                                            ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]

let private renderPatientInfo { personalInfo = personalInfo; historyInfo = historyInfo } =
    React.fragment [
        match personalInfo with
        | Some info ->
            Html.div [
                prop.custom("data-aos", "fade-down")
                prop.custom("data-aos-offset", "100")
                prop.className [ Css.Box; Css.Mt4; Css.Mx2 ]
                prop.children [
                    Html.nav [
                        prop.className Css.Level
                        prop.children [
                            Html.div [
                                prop.className [ Css.LevelItem; Css.HasTextCentered ]
                                prop.children [
                                    Html.div [
                                        Html.p [
                                            prop.className Css.Heading
                                            prop.text "Nombre completo"
                                        ]
                                        Html.p [
                                            prop.className [ Css.Title; Css.Is5 ]
                                            prop.text (
                                                info.fullname
                                            )
                                        ]
                                    ]
                                ]
                            ]
                            Html.div [
                                prop.className [ Css.LevelItem; Css.HasTextCentered ]
                                prop.children [
                                    Html.div [
                                        Html.p [
                                            prop.className Css.Heading
                                            prop.text "Cédula"
                                        ]
                                        Html.p [
                                            prop.className [ Css.Title; Css.Is5 ]
                                            prop.text info.cid
                                        ]
                                    ]
                                ]
                            ]
                            Html.div [
                                prop.className [ Css.LevelItem; Css.HasTextCentered ]
                                prop.children [
                                    Html.div [
                                        Html.p [
                                            prop.className Css.Heading
                                            prop.text "Edad"
                                        ]
                                        Html.p [
                                            prop.className [ Css.Title; Css.Is5 ]
                                            prop.text info.age
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        | None ->
            Html.div [
                prop.className [ Css.Py4; Css.IsFlex; Css.IsJustifyContentCenter ]
                prop.children [
                    Html.span [
                        prop.className [ Css.Icon; Css.IsLarge ]
                        prop.children [
                            Html.i [
                                prop.className [ Css.Fa
                                                 Icons.Fa2X
                                                 Icons.FaSpinner
                                                 Icons.FaPulse ]
                            ]
                        ]
                    ]
                ]
            ]

        match historyInfo with
        | Some userHistories ->
            React.fragment [
                for userHistory in userHistories do
                renderUserHistory userHistory
            ]
            Html.div [
                prop.className Css.PanelBlock
                prop.children [
                    Html.button [
                        prop.className [ Css.Button
                                         Css.IsFullwidth
                                         "fbutton" ]
                        prop.text ("Registrar nueva historia")
                    ]
                ]
            ]
        | None ->
            Html.div [
                prop.className [ Css.Py4; Css.IsFlex; Css.IsJustifyContentCenter ]
                prop.children [
                    Html.span [
                        prop.className [ Css.Icon; Css.IsLarge ]
                        prop.children [
                            Html.i [
                                prop.className [ Css.Fa
                                                 Icons.Fa2X
                                                 Icons.FaSpinner
                                                 Icons.FaPulse ]
                            ]
                        ]
                    ]
                ]
            ]
    ]

let private renderServiceData serviceData =
    React.fragment [
    match serviceData with
    | None ->
        Html.div [
            prop.className [ Css.Py4; Css.IsFlex; Css.IsJustifyContentCenter ]
            prop.children [
                Html.span [
                    prop.className [ Css.Icon; Css.IsLarge ]
                    prop.children [
                        Html.i [
                            prop.className [ Css.Fa
                                             Icons.Fa2X
                                             Icons.FaSpinner
                                             Icons.FaPulse ]
                        ]
                    ]
                ]
            ]
        ]
    | Some services ->
        for service in services do
        Html.a [
            prop.className Css.PanelBlock
            prop.custom("data-aos", "fade-down")
            prop.custom("data-aos-offset", "100")
            prop.children [
                Html.span [
                    prop.className Css.PanelIcon
                    prop.children [
                        Html.i [
                            prop.className [ yield Css.Fa
                                             match service.inactive with
                                             | true -> yield! [ Icons.FaAddressBook; Css.HasTextWarning ]
                                             | false -> yield! [ Icons.FaCheck; Css.HasTextSuccess ]
                                            ]
                        ]
                    ]
                ]
                Html.text (
                    sprintf "Servicio %s (%.2f BsS)"
                    <| service.name
                    <| service.price
                )
        ]
    ]
    ]
[<ReactComponent>]
let private ConsultationUpdate _ =
    let close = React.useContext(modalContext)
    let {
            postingStatus = postingStatus
            errors = errors
            issueSearch = issueSearch
            formValue = formValue
            setFormValue = setFormValue
        } = React.useContext(formContext)
    let tabs = [ "Actualizar Datos"
                 "Datos de Paciente"
                 "Servicios Prestados" ]
    let (selectedTab, setSelectedTab) = React.useState(tabs.[0])
    let (patientInfo, setPatientInfo) = React.useState({ personalInfo = None;
                                                         historyInfo = None })
    let (consultationServices, setConsultationServices) = React.useState(None)
    let currentIndex = List.findIndex ((=) selectedTab) tabs
    let consultation = downcast formValue
    let setConsultation = downcast setFormValue
    let disabableProps = Helpers.disabableProps
    let errorProps = Helpers.errorProps
    let hasErrors key = Helpers.hasErrors key errors
    let isPosting = Helpers.isPosting postingStatus
    React.useEffect((fun () ->
        if selectedTab = tabs.[1] then
            async {
                do setPatientInfo { personalInfo = None; historyInfo = None }
                let! patient =
                    Fetch.get(sprintf "http://192.168.1.7:3000/api/consultation/patient_information/%d" consultation.id,
                              decoder = PatientManagement.userRecordDecoder)
                    |> Async.AwaitPromise
                let infoS1 = { patientInfo with personalInfo = Some patient }
                do setPatientInfo infoS1
                let! histories =
                    Fetch.get(sprintf "http://192.168.1.7:3000/api/patient/history/%d" patient.cid,
                              decoder = Decode.list PatientManagement.historyDecoder)
                    |> Async.AwaitPromise
                do setPatientInfo { infoS1 with historyInfo = Some histories }
            } |> Async.Start
        else if selectedTab = tabs.[2] then
            async {
                do setConsultationServices None
                let! services =
                    Fetch.get(sprintf "http://192.168.1.7:3000/api/consultation/services/%d" consultation.id,
                              decoder = Decode.list serviceDecoder)
                    |> Async.AwaitPromise
                do setConsultationServices <| Some services
            } |> Async.Start
    ), [| box selectedTab |])
    React.fragment [
        Html.div [
            prop.className Css.ModalContent
            prop.children [
                Html.nav [
                    prop.className [ Css.Panel; Css.IsDark; Css.Card; Css.M2 ]
                    prop.children [
                        Html.p [
                            prop.className Css.PanelHeading
                            prop.text "Información de Paciente"
                        ]
                        if postingStatus = Posted || Map.containsKey "posting" errors then
                            Html.div [
                                prop.className [ Css.PanelBlock ]
                                prop.children [
                                    Html.article [
                                        prop.className [ Css.Message
                                                         "fmessage"
                                                         if postingStatus = Posted then
                                                            Css.IsSuccess
                                                         else
                                                            Css.IsDanger
                                                        ]
                                        prop.children [
                                            Html.div [
                                                prop.className Css.MessageBody
                                                prop.text (
                                                    if postingStatus = Posted then
                                                        "Los datos del usuario han sido actualizados"
                                                    else
                                                        sprintf "No se pudo completar la solicitud: %s" errors.["posting"]
                                                )
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        Html.div [
                            prop.className [ Css.PanelTabs; Css.Mt3 ]
                            prop.children [
                                for tab in tabs do
                                Html.a [
                                    prop.text tab
                                    prop.disabled <| (postingStatus = Posting)
                                    if tab = selectedTab then prop.className Css.IsActive
                                    prop.onClick (fun _ -> setSelectedTab tab)
                                ]
                            ]
                        ]
                        match currentIndex with
                        | 1 -> renderPatientInfo patientInfo
                        | 2 -> renderServiceData consultationServices
                        | 0 ->
                            React.fragment [
                            Html.div [
                                prop.className [
                                    Css.PanelBlock
                                    Css.IsJustifyContentCenter
                                ]
                                prop.children [
                                    Html.div [
                                        prop.className [
                                            Css.Container
                                            Css.IsFullwidth
                                        ]
                                        prop.children [
                                            ConsultationForm()
                                            Html.div [
                                                prop.className [ Css.Field; Css.IsHorizontal ]
                                                prop.children [
                                                    Html.div [
                                                        prop.className Css.FieldBody
                                                        prop.children [
                                                            Html.div [
                                                                prop.className Css.Field
                                                                prop.children [
                                                                    Html.div [
                                                                        prop.className [ Css.Control; Css.IsExpanded ]
                                                                        prop.children [
                                                                            Html.p [
                                                                                prop.className [ Css.Select; Css.IsFullwidth ]
                                                                                prop.children [
                                                                                    Html.select [
                                                                                        prop.value ( sprintf "Estado: %s" <| consultation.status.ToString() )
                                                                                        prop.onChange (fun state ->
                                                                                            let index =
                                                                                                [ InProcess; Accepted; Cancelled ]
                                                                                                |> List.map ((fun st -> st.ToString()) >> sprintf "Estado: %s")
                                                                                                |> List.findIndex ((=) state)
                                                                                            match index with
                                                                                            | 0 ->
                                                                                                setConsultation { consultation with status = InProcess }
                                                                                            | 1 ->
                                                                                                setConsultation { consultation with status = Accepted }
                                                                                            | 2 ->
                                                                                                setConsultation { consultation with status = Cancelled }
                                                                                            | _ ->
                                                                                                failwith <| sprintf "Estado no soportado: %d" index
                                                                                        )
                                                                                        prop.className [ Css.IsFullwidth ]
                                                                                        prop.children [
                                                                                            for status in [ InProcess; Cancelled; Accepted ] do
                                                                                            Html.option(sprintf "Estado: %s" <| status.ToString())
                                                                                        ]
                                                                                    ]
                                                                                ]
                                                                            ]
                                                                        ]
                                                                    ]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                            Html.div [
                                prop.className Css.PanelBlock
                                prop.children [
                                    Html.button [
                                        prop.className [ Css.Button
                                                         Css.IsFullwidth
                                                         "fbutton"
                                                         if postingStatus = Posting then
                                                            Css.IsLoading ]
                                        prop.text "Actualizar"
                                        prop.disabled <| (postingStatus = Posting)
                                        prop.onClick (fun _ -> if currentIndex = 0 && not isPosting then issueSearch())
                                    ]
                                ]
                            ]
                            ]
                        | _ -> Html.none
                    ]
                ]
                Html.button [
                    prop.className [ Css.ModalClose; Css.IsLarge ]
                    prop.onClick (fun _ -> if postingStatus <> Posting then close())
                ]
            ]
        ]
    ]

let private consultationDecoder =
    Decode.object (fun get -> {
        patientCid = get.Required.Field "patient_id" Decode.int
        status =
            let status = get.Required.Field "status" Decode.int
            match status with
            | 1 -> Cancelled
            | 2 -> InProcess
            | _ -> Accepted
        consultationDate =
            get.Required.Field "consultation_date" Decode.datetime
        reason =
            get.Required.Field "reason" Decode.string
        id =
            get.Required.Field "id" Decode.int
    })

[<ReactComponent>]
let rec ConsultationIndex() =
    StreamIndex [
        StreamIndexProps.storeName "Consultation"
        StreamIndexProps.packageEncoder
        <| Encode.Auto.generateEncoder<ConsultationData>()
        StreamIndexProps.title (
            Html.p [
                prop.className Css.Title
                prop.text "Manejo de Visitas y Citas"
            ]
        )
        StreamIndexProps.subtitle (
            Html.div [ prop.className Css.Content; prop.children [
                Html.p [
                    prop.className [ Css.Subtitle; Css.Mt1 ]
                    prop.text """En esta vista se realiza el manejo
                            de las citas y visitas que realizan los pacientes
                            de la fundación.
                            Para el rol actual, se permiten las siguientes actividades"""
                ]
                Html.ul [
                    prop.children [
                        for act in [ "Registrar visita/cita"
                                     "Actualizar visita/cita"
                                     "Consultar visita/cita" ] do
                        Html.li act
                    ]
                ]
            ]]
        )
        StreamIndexProps.wsUrl "ws://192.168.1.7:3000/api/consultation/stream"
        StreamIndexProps.queryEncoder encodePackage
        StreamIndexProps.packageDecoder
        <| Decode.oneOf [ consultationDecoder; Decode.Auto.generateDecoder() ]
        StreamIndexProps.packageDescriptor
        <| (fun desc pak ->
                match desc with
                | Cid ->
                    Html.td pak.patientCid
                | ConsultationDate ->
                    Html.td(pak.consultationDate.ToString("dd/MM/yyyy"))
                | Status ->
                    Html.td(pak.status.ToString())
                | Reason ->
                    Html.td (
                        if pak.reason.Length >= 50
                        then (sprintf "%s..." <| pak.reason.Substring(0, 47))
                        else pak.reason
                    )
                | _ -> Html.none)
        StreamIndexProps.descriptors [
            Cid, "Cédula del paciente"
            Status, "Estatus de la Visita/Cita"
            ConsultationDate, "Fecha de la Visita/Cita"
            Reason, "Razón de la visita"
            All, "Todos"
        ]
        StreamIndexProps.onAddComponent (
            ModalForm [
                ModalFormProps.url "http://192.168.1.7:3000/api/consultation/create"
                ModalFormProps.encoder encodeConsultation
                ModalFormProps.initialValue {
                    patientCid = 0
                    consultationDate = DateTime(0, 0, 0)
                    status = InProcess
                    reason = ""
                    id = 0
                }
                ModalFormProps.rules (
                    let dummy = Unchecked.defaultof<ConsultationData> in
                    [
                        ModalFormProps.rule <| nameof dummy.consultationDate
                                            <| ( mustNotBeEmpty (fun consultation ->
                                                          consultation.consultationDate)
                                                 <| DateTime(0, 0, 0)
                                                 <| "La fecha no puede estar vacía"
                                                 >=> mustBeTodayOrPosterior )
                        ModalFormProps.rule <| nameof dummy.patientCid
                                            <| ( mustNotBeEmpty (fun consultation ->
                                                          consultation.patientCid )
                                                <| 0
                                                <| "La cédula del paciente no puede estar vacía")
                        ModalFormProps.rule <| nameof dummy.reason
                                            <| ( mustNotBeEmpty (fun consultation ->
                                                            consultation.reason )
                                                 <| ""
                                                 <| "La razón de visita del paciente no puede estar vacía")
                    ])
                ModalFormProps.renderer
                    << ModalDecorator "Nueva Visita/Cita" "Agregar Visita/Cita"
                    <| ConsultationForm()
                ]
            )
        StreamIndexProps.clickModalComponent(fun consultation ->
            ModalForm [
                ModalFormProps.url "http://192.168.1.7:3000/api/consultation/update"
                ModalFormProps.encoder updateRequest
                ModalFormProps.initialValue consultation
                ModalFormProps.rules (
                    let dummy = Unchecked.defaultof<ConsultationData> in
                    [
                        ModalFormProps.rule <| nameof dummy.consultationDate
                                            <| ( mustNotBeEmpty (fun consultation ->
                                                          consultation.consultationDate)
                                                 <| DateTime(0, 0, 0)
                                                 <| "La fecha no puede estar vacía")
                        ModalFormProps.rule <| nameof dummy.patientCid
                                            <| ( mustNotBeEmpty (fun consultation ->
                                                          consultation.patientCid )
                                                <| 0
                                                <| "La cédula del paciente no puede estar vacía")
                        ModalFormProps.rule <| nameof dummy.reason
                                            <| ( mustNotBeEmpty (fun consultation ->
                                                            consultation.reason )
                                                 <| ""
                                                 <| "La razón de visita del paciente no puede estar vacía")
                    ])
                ModalFormProps.renderer
                    <| ConsultationUpdate consultation
                ]
        )
    ]
