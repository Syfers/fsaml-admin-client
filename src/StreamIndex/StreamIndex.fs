module StreamIndex

open Fable
open Feliz
open FSharpPlus.Operators
open StreamProtocol
open FSharpPlus.Data
open StreamIndexComponents
open Thoth.Json

[<ReactComponent>]
let private StreamIndexComponent propOptions =
    let protocolContext = React.useContext(wsProtocolContext)
    let protocolState  = downcast protocolContext.protocolState
    let (activeAddModal, setActiveAddModal) = React.useState(false)
    let toggleAddModal =
        React.useCallback(
            (fun () -> setActiveAddModal <| not activeAddModal),
            [| box setActiveAddModal ; box activeAddModal |]
        )
    let searchState = protocolState.searchState
    let queryPages = protocolState.currentPages
    let query = protocolState.query
    let setQuery = downcast protocolContext.setQuery
    let dispatchLayoutChanged = React.useContext(Layout.oneColumnLayoutContext)
    let (selectedPackage, setSelectedPackage) = React.useState(None);
    React.useEffect(
        ( fun () ->
            if searchState.IsResultOrSearching && not searchState.InSearch then
                do dispatchLayoutChanged Layout.Expanded
            else
                do dispatchLayoutChanged Layout.Collapsed
          )
        ,[| box searchState |])
    Html.div [
        propOptions.title
        propOptions.subtitle
        StreamIndexComponents.SearchForm
        <| (fun newSearch ->
            setQuery { query with search = newSearch; pageSearch = 1 })
        <| (propOptions.displayToDescriptor
            >> fun newType -> setQuery { query with ``type`` = newType; pageSearch = 1 })
        <| Seq.ofList(propOptions.descriptorsNames)
        <| toggleAddModal
        <| searchState
        <| query.search
        <| propOptions.descriptorToDisplay query.``type``
        ModalComponent ( propOptions.addModalComponent ) activeAddModal toggleAddModal
        ModalComponent
        <| ( match selectedPackage with
             | Some pkg -> propOptions.clickModalComponent pkg
             | None -> Html.none )
        <| Option.isSome selectedPackage
        <| (fun () -> setSelectedPackage None)
        Html.section [
            prop.className [ Css.Section
                             Css.My1
                             Css.Px0
                             Css.Block ]
            prop.children (
                renderStreamProtocolState
                <| propOptions
                <| searchState
                <| query
                <| queryPages
                <| setQuery
                <| ( setSelectedPackage << Some )
            )
        ]
    ]

[<ReactComponent>]
let StreamIndex props =
    let propOptions =
        StreamIndexProps.build
        <| props
        <| {
        _title =
            Html.p [
                prop.className Css.Title
                prop.text "StreamIndex"
            ]
        _subtitle =
            Html.p [
                prop.className Css.Title
                prop.text "StreamIndex Description"
            ]
        _packageDecoder = None
        _queryEncoder = None
        _packageDescriptor = None
        _descriptorToDisplay = None
        _displayToDescriptor = None
        _descriptorsNames = [  ]
        _addModalComponent = Html.none
        _clickModalComponent = fun _ -> Html.none
        _wsUrl = null
        _storeName = ""
        _packageEncoder = konst Encode.nil
    }
    let options = {
            title = propOptions._title
            subtitle = propOptions._subtitle
            packageDecoder = Option.get <| propOptions._packageDecoder
            packageDescriptor = Option.get <| propOptions._packageDescriptor
            queryEncoder = Option.get <| propOptions._queryEncoder
            descriptorToDisplay = Option.get <| propOptions._descriptorToDisplay
            displayToDescriptor = Option.get <| propOptions._displayToDescriptor
            descriptorsNames = propOptions._descriptorsNames
            wsUrl = propOptions._wsUrl
            addModalComponent = propOptions._addModalComponent
            clickModalComponent = propOptions._clickModalComponent
            storeName = propOptions._storeName
            packageEncoder = propOptions._packageEncoder
        }
    WebSocketProvider options.wsUrl [
        StreamProtocol [
            StreamProtocolProps.storeName options.storeName
            StreamProtocolProps.packageEncoder options.packageEncoder
            StreamProtocolProps.url options.wsUrl
            StreamProtocolProps.descriptorToDisplay options.descriptorToDisplay
            StreamProtocolProps.displayToDescriptor options.displayToDescriptor
            StreamProtocolProps.initialDescriptor
                <| options.displayToDescriptor(options.descriptorsNames.Head)
            StreamProtocolProps.packageDecoder
                <| options.packageDecoder
            StreamProtocolProps.queryEncoder options.queryEncoder
            StreamProtocolProps.descriptorToDisplay options.descriptorToDisplay
            StreamProtocolProps.children [ StreamIndexComponent options ]
        ]
    ]
