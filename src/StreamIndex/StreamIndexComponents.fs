module StreamIndexComponents
open Feliz
open StreamProtocol
open Thoth.Json
open FSharpPlus.Data
open FSharpPlus.Operators
open System

type StreamIndexInputOptions<'pkg, 'queryType> = {
    _title: ReactElement
    _subtitle: ReactElement
    _packageDecoder: Option<Decoder<'pkg>>
    _queryEncoder: Option<Query<'queryType> -> int -> JsonValue>
    _packageDescriptor: Option<'queryType -> 'pkg -> ReactElement>
    _descriptorToDisplay: Option<'queryType -> string>
    _displayToDescriptor: Option<string -> 'queryType>
    _descriptorsNames: string list
    _addModalComponent: ReactElement
    _clickModalComponent: 'pkg -> ReactElement
    _wsUrl: string
    _storeName: string
    _packageEncoder: 'pkg -> JsonValue
}

type StreamIndexOptions<'pkg, 'queryType> = {
    title: ReactElement
    subtitle: ReactElement
    packageDecoder: Decoder<'pkg>
    queryEncoder: Query<'queryType> -> int -> JsonValue
    packageDescriptor: 'queryType -> 'pkg -> ReactElement
    descriptorToDisplay: 'queryType -> string
    displayToDescriptor: string -> 'queryType
    descriptorsNames: string list
    addModalComponent: ReactElement
    clickModalComponent: 'pkg -> ReactElement
    storeName: string
    packageEncoder: 'pkg -> JsonValue
    wsUrl: string
}

module StreamIndexProps =
    let title title =
        State.modify <| fun options -> { options with _title = title }
    let onAddComponent comp =
        State.modify <| fun options -> { options with _addModalComponent = comp }
    let clickModalComponent comp =
        State.modify <| fun options -> { options with _clickModalComponent = comp }
    let subtitle subtitle =
        State.modify <| fun options -> { options with _subtitle = subtitle }
    let packageDecoder decoder: State<StreamIndexInputOptions<_,_>,_> =
        State.modify <| fun options ->
            { options with _packageDecoder = Some decoder }
    let queryEncoder encoder: State<StreamIndexInputOptions<_,_>,_> =
        State.modify <| fun options ->
            { options with _queryEncoder = Some encoder }
    let packageDescriptor descriptor =
        State.modify <| fun options -> { options with _packageDescriptor = Some descriptor }
    let storeName name: State<StreamIndexInputOptions<_,_>,_> =
        State.modify
        <| fun options ->
            { options with _storeName = name }
    let packageEncoder enc: State<StreamIndexInputOptions<_,_>,_> =
        State.modify
        <| fun options ->
            { options with _packageEncoder = enc }
    let descriptors descriptors =
        let map = Map.ofList descriptors
        let inverseMap = Map.ofList <| List.map (fun (a, b) -> b, a) descriptors
        State.modify <| fun options ->
            { options with
                _descriptorToDisplay = Some <| fun k -> map.[k]
                _displayToDescriptor = Some <| fun k -> inverseMap.[k]
                _descriptorsNames = List.map snd descriptors
            }
    let wsUrl url : State<StreamIndexInputOptions<_,_>,_> =
        State.modify <| fun options -> { options with _wsUrl = url }
    let build props options =
        List.fold (flip State.exec) options props

let Paginator queryPages query setQuery =
    Html.nav [
        prop.className [ Css.Pagination; Css.IsCentered ]
        prop.role "navigation"
        prop.ariaLabel "pagination"
        prop.custom("data-aos", "fade-in")
        prop.children [
            yield Html.a [
                prop.className Css.PaginationPrevious
                prop.text "Anterior"
                prop.onClick (fun _ ->
                              if query.pageSearch > 1 then
                                setQuery { query with pageSearch = query.pageSearch - 1 })
            ]
            yield Html.a [
                prop.className Css.PaginationNext
                prop.text "Siguiente"
                prop.onClick (fun _ ->
                              if query.pageSearch < queryPages then
                                setQuery { query with pageSearch = query.pageSearch + 1 })
            ]
            yield Html.ul [
                prop.className Css.PaginationList
                prop.children [
                    if queryPages <= 10 then
                        for index in 1..queryPages do
                        yield Html.li [
                                Html.a [
                                    prop.className (
                                        if query.pageSearch = index then
                                            [ Css.PaginationLink; Css.IsCurrent ]
                                        else
                                            [ Css.PaginationLink ]
                                    )
                                    prop.onClick (fun _ -> setQuery { query with pageSearch = index })
                                    prop.text index
                                ]
                        ]
                    else
                        for index in 1..3 do
                            yield (Html.li: ReactElement list -> ReactElement) << List.singleton
                            <| Html.a [
                                prop.className (
                                    if query.pageSearch = index then
                                        [ Css.PaginationLink; Css.IsCurrent ]
                                    else
                                        [ Css.PaginationLink ]
                                )
                                prop.onClick (fun e ->
                                    e.preventDefault()
                                    setQuery { query with pageSearch = index })
                                prop.text index
                            ]
                        yield (Html.li: ReactElement list -> ReactElement) << List.singleton
                        <| Html.span [
                            prop.className Css.PaginationEllipsis
                            prop.text "..."
                        ]
                        if not <| List.contains query.pageSearch [ 1; 2; 3; queryPages; queryPages - 1; queryPages - 2 ] then
                            yield! [
                                (Html.li: ReactElement list -> ReactElement) << List.singleton
                                <| Html.a [
                                    prop.className [ Css.PaginationLink; Css.IsCurrent ]
                                    prop.text query.pageSearch
                                ]
                                (Html.li: ReactElement list -> ReactElement) << List.singleton
                                <| Html.span [
                                    prop.className Css.PaginationEllipsis
                                    prop.text "..."
                                ]
                            ]
                        for index in 0..2 do
                            yield (Html.li: ReactElement list -> ReactElement) << List.singleton
                            <| Html.a [
                                prop.className (
                                    if query.pageSearch = queryPages - ( 2 - index ) then
                                        [ Css.PaginationLink; Css.IsCurrent ]
                                    else
                                        [ Css.PaginationLink ]
                                )
                                prop.onClick (fun _ ->
                                    setQuery { query with pageSearch = queryPages - ( 2 - index ) })
                                prop.text (queryPages - ( 2 - index ))
                            ]

                ]
            ]
        ]
    ]

let SearchForm (onTextChanged: string -> unit) (onTypeChanged: string -> unit) options onAdd (searchState:SearchState<_>) search queryType =
    Html.div [
        Html.div [
            prop.className [ Css.IsHiddenMobile ]
            prop.children [
                Html.div [
                    prop.className [ Css.Field; Css.IsHorizontal; Css.HasAddons; Css.HasAddonsCentered ]
                    prop.children [
                        Html.p [
                            prop.className Css.Control
                            prop.children [
                                Html.span [
                                    prop.className [ Css.Select; Css.IsMedium ]
                                    prop.children [
                                        Html.select [
                                            (prop.value: string -> _) queryType
                                            prop.onChange onTypeChanged
                                            prop.children [
                                                    for item in options do
                                                    (Html.option: string -> ReactElement) item
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                        Html.p [
                            prop.className [ Css.Control
                                             Css.IsExpanded
                                             Css.IsMedium
                                             if searchState.InSearch then
                                                Css.IsLoading
                                            ]
                            prop.children [
                                Html.input [
                                    prop.className [ Css.Input
                                                     Css.IsMedium ]
                                    prop.type' "text"
                                    (prop.value: string -> _) search
                                    prop.onChange(onTextChanged)
                                ]
                            ]
                        ]
                        Html.p [
                            prop.className [ Css.Control ]
                            prop.children [
                                Html.button [
                                    prop.className [ Css.Button; Css.IsMedium; "fbutton" ]
                                    prop.onClick (fun _ -> onAdd())
                                    prop.text "Agregar"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
        Html.div [
            prop.className [ Css.IsHiddenTablet ]
            prop.children [
                Html.div [
                    prop.className [ Css.Field; Css.IsHorizontal ]
                    prop.children [
                        Html.div [
                            prop.className [ Css.FieldLabel; Css.IsNormal ]
                            prop.children [
                                Html.label [
                                    prop.className Css.Label
                                    prop.text "Campo de búsqueda"
                                ]
                            ]
                        ]
                        Html.div [
                            prop.className Css.FieldBody
                            prop.children [
                                Html.div [
                                    prop.className Css.Field
                                    prop.children [
                                        Html.p [
                                            prop.className [ Css.Control; Css.IsExpanded; Css.IsMobile ]
                                            prop.children [
                                                Html.span [
                                                    prop.className Css.Select
                                                    prop.children [
                                                        Html.select [
                                                            prop.onChange onTypeChanged
                                                            prop.value queryType
                                                            prop.children [
                                                                    for item in options do
                                                                    Html.option item
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
                Html.div [
                    prop.className [ Css.Field; Css.IsHorizontal ]
                    prop.children [
                        Html.div [
                            prop.className [ Css.FieldLabel; Css.IsNormal ]
                            prop.children [
                                Html.label [
                                    prop.className Css.Label
                                    prop.text "Texto de búsqueda"
                                ]
                            ]
                        ]
                        Html.div [
                            prop.className Css.FieldBody
                            prop.children [
                                Html.div [
                                    prop.className Css.Field
                                    prop.children [
                                        Html.p [
                                            prop.className [ Css.Control
                                                             Css.IsExpanded
                                                             Css.HasIconsLeft
                                                             if searchState.InSearch then
                                                                Css.IsLoading
                                                             ]
                                            prop.children [
                                                Html.input [
                                                    prop.value search
                                                    prop.className [
                                                        Css.Input
                                                    ]
                                                    prop.onChange onTextChanged
                                                    prop.className Css.Input
                                                ]
                                                Html.span [
                                                    prop.className [ Css.Icon; Css.IsLeft ]
                                                    prop.children [
                                                        Html.i [ prop.className [ Css.Fa; Icons.FaSearch ] ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
                Html.div [
                    prop.className [ Css.Field; Css.IsHorizontal ]
                    prop.children [
                        Html.div [
                            prop.className Css.FieldBody
                            prop.children [
                                Html.div [
                                    prop.className Css.Field
                                    prop.children [
                                        Html.p [
                                            prop.className [ Css.Control; Css.Mt5
                                                             Css.IsExpanded; Css.IsFullwidth ]
                                            prop.children [
                                                Html.button [
                                                    prop.text "Agregar"
                                                    prop.onClick (fun _ -> onAdd())
                                                    prop.className [ Css.Button
                                                                     Css.IsFullwidth; "fbutton" ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]

let rec renderStreamProtocolState propOptions searchState query queryPages setQuery setPackage =
    [
    match searchState with
    | SearchError(msg, prevState) ->
        yield Html.article [
            prop.className [ Css.Message; Css.IsDanger ]
            prop.children [
                Html.div [
                    prop.className Css.MessageBody
                    prop.text msg
                ]
            ]
        ]
        yield!
            renderStreamProtocolState propOptions prevState query queryPages setQuery setPackage
    | Searching prevState ->
        yield!
            renderStreamProtocolState propOptions prevState query queryPages setQuery setPackage
    | NoResult msg ->
        yield Html.article [
            prop.className [ Css.Message; Css.IsDark ]
            prop.children [
                Html.div [
                    prop.className Css.MessageHeader
                    prop.children [
                        Html.p [ prop.text "Sin Resultados" ]
                    ]
                ]
                Html.div [
                    prop.className Css.MessageBody
                    prop.text msg
                ]
            ]
        ]
    | Result queryResult ->
        yield Paginator queryPages query setQuery
        yield Html.table [
            prop.className [ Css.Table
                             Css.IsFullwidth
                             Css.IsHoverable
                             Css.IsStriped
                             Css.IsFullheight ]
            prop.children [
                Html.thead [
                    prop.children [
                        Html.tr [
                            for desc in propOptions.descriptorsNames do
                                if desc <> "Todos" then
                                    Html.th desc
                        ]
                    ]
                ]
                Html.tbody [
                    for package in queryResult do
                    Html.tr [
                        prop.custom("data-aos", "fade-right")
                        prop.className Css.IsClickable
                        prop.onClick (fun _ -> setPackage package)
                        prop.children [
                            let props = propOptions
                            for desc in props.descriptorsNames do
                                if desc <> "Todos" then
                                    props.packageDescriptor
                                    <| (props.displayToDescriptor desc)
                                    <| package
                        ]
                    ]
                ]
            ]
        ] |> (fun table ->
              Html.div [
                prop.className "ftable-container"
                prop.children [ table ]
              ])
        yield Html.div [
                prop.className Css.Mt3
                prop.children [ Paginator queryPages query setQuery ]
            ]
    ]

let modalContext = React.createContext("modalContext")

[<ReactComponent>]
let ModalComponent content isActive (onClick: unit -> unit) =
    if isActive then
        React.contextProvider(
            modalContext,
            onClick,
            Html.div [
                prop.className [ Css.Modal; Css.IsActive ]
                prop.children [
                    Html.div [
                        prop.className Css.ModalBackground
                    ]
                    content
                ]
            ])
    else
        Html.none
