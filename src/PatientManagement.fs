module PatientManagement

open Feliz
open System
open ModalForm
open Fable.Core
open StreamProtocol
open StreamIndex
open StreamIndexComponents
open FSharpPlus
open Thoth.Json
open Thoth.Fetch
open Browser.Types
open Fetch

type UserRecord = {
    fullname: string
    address: string
    age: int
    cid: int
    birthdate: DateTime
}

type private UserData = {
    names: string
    lastnames: string
    address: string
    birthdate: DateTime
    cid: int
    previousCid: int
}

type HistoryStatus =
    | InProgress
    | Completed
    | Cancelled

type History = {
    status: HistoryStatus
    date: DateTime
}

type private QueryType =
    | Cid
    | Fullname
    | Address
    | Birthdate
    | All

let private emptyField field msg pkg =
    if not << String.IsNullOrEmpty <| field pkg then
        Ok pkg
    else
        Error msg

let private maxLength (field: 'a -> string) length msg pkg =
    if (field pkg).Length <= length then
        Ok pkg
    else
        Error msg

let private userDataEncoder userData =
    Encode.object [
        "cid", Encode.int userData.cid
        "names", Encode.string userData.names
        "lastnames", Encode.string userData.lastnames
        "birthdate", Encode.string <| userData.birthdate.ToString("yyyy-MM-dd")
        "address", Encode.string userData.address
    ]

let userRecordDecoder =
    Decode.object
        (fun get ->
        {
            cid = get.Required.Field "cid" Decode.int
            fullname =
                sprintf "%s, %s"
                <| (get.Required.Field "names" Decode.string)
                <| (get.Required.Field "lastnames" Decode.string)
            address = get.Required.Field "address" Decode.string
            age =
                let zero = DateTime(1, 1, 1)
                let difference = (DateTime.Now) - (get.Required.Field "birthdate" Decode.datetime)
                (zero + difference).Year - 1
            birthdate = get.Required.Field "birthdate" Decode.datetime
        }
    )

let historyDecoder =
    Decode.object (
        fun get ->
        {
            status =
                match get.Required.Field "status" Decode.string with
                | "in-progress" -> InProgress
                | "completed" -> Completed
                | _ -> Cancelled

            date = get.Required.Field "date" Decode.datetime
        })

let private queryEncoder query queryNum=
    Encode.object [
        "type", Encode.string <| query.``type``.ToString()
        "operation",
        Encode.string
            <| if query.``type`` = Birthdate then "is_of_age" else "is_like"
        "search", Encode.string query.search
        "query_id", Encode.int queryNum
        "page", Encode.int query.pageSearch
    ]


[<ReactComponent>]
let private UserForm() =
    let { postingStatus = postingStatus
          formValue = formValue
          setFormValue = setFormValue
          errors = errors
          issueSearch = issueSearch
        } = React.useContext(formContext)
    let user = downcast formValue
    let setUser = downcast setFormValue
    let disabableProps = Helpers.disabableProps
    let errorProps = Helpers.errorProps
    let hasErrors key = Helpers.hasErrors key errors
    let isPosting = Helpers.isPosting postingStatus
    React.fragment [
                    Html.div [
                        prop.className [ Css.Field; Css.IsHorizontal ]
                        prop.children [
                            Html.div [
                                prop.className Css.FieldBody
                                prop.children [
                                    Html.div [
                                        prop.className Css.Field
                                        prop.children [
                                            Html.div [
                                                prop.className [ Css.Control; Css.HasIconsLeft ]
                                                prop.children [
                                                    Html.input [
                                                        prop.className (
                                                            [ Css.Input ]
                                                            |> errorProps (hasErrors "cid")
                                                        )
                                                        prop.disabled isPosting
                                                        prop.type' "number"
                                                        prop.placeholder "Cédula"
                                                        prop.value (
                                                            if user.cid = 0 then
                                                                ""
                                                            else
                                                                user.cid.ToString()
                                                        )
                                                        (prop.onChange: (string -> unit) -> _)
                                                        <| (fun cid ->
                                                            setUser { user with cid = (if cid = "" then 0 else Int32.Parse cid) })
                                                    ]
                                                    Html.span [
                                                        prop.className [ Css.Icon; Css.IsLeft ]
                                                        prop.children [
                                                            Html.i [
                                                                prop.className [
                                                                    Css.Fa
                                                                    Icons.FaUser
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                    if hasErrors "cid" then
                                                        Html. p [
                                                            prop.className [ Css.Help; Css.IsDanger ]
                                                            prop.text errors.["cid"]
                                                        ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                    Html.div [
                        prop.className [ Css.Field; Css.IsHorizontal ]
                        prop.children [
                            Html.div [
                                prop.className Css.FieldBody
                                prop.children [
                                    Html.div [
                                        prop.className Css.Field
                                        prop.children [
                                            Html.div [
                                                prop.className Css.Control
                                                prop.children [
                                                    Html.input [
                                                        prop.className (
                                                            [ Css.Input ]
                                                            |> errorProps (hasErrors "names")
                                                        )
                                                        prop.disabled isPosting
                                                        prop.value user.names
                                                        prop.placeholder "Nombres"
                                                        (prop.onChange: (string -> unit) -> _)
                                                        <| (fun names -> setUser { user with names = names })
                                                    ]
                                                    if hasErrors "names" then
                                                        Html. p [
                                                            prop.className [ Css.Help; Css.IsDanger ]
                                                            prop.text errors.["names"]
                                                        ]
                                                ]
                                            ]
                                        ]
                                    ]
                                    Html.div [
                                        prop.className Css.Field
                                        prop.children [
                                            Html.div [
                                                prop.className Css.Control
                                                prop.children [
                                                    Html.input [
                                                        prop.className (
                                                            [ Css.Input ]
                                                            |> errorProps (hasErrors "lastnames")
                                                        )
                                                        prop.disabled isPosting
                                                        prop.value user.lastnames
                                                        prop.placeholder "Apellidos"
                                                        (prop.onChange: (string -> unit) -> _)
                                                        <| (fun lastnames -> setUser { user with lastnames = lastnames })
                                                    ]
                                                    if hasErrors "lastnames" then
                                                        Html. p [
                                                            prop.className [ Css.Help; Css.IsDanger ]
                                                            prop.text errors.["lastnames"]
                                                        ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                    Html.div [
                        prop.className [ Css.Field; Css.IsHorizontal ]
                        prop.children [
                            Html.div [
                                prop.className Css.FieldBody
                                prop.children [
                                    Html.div [
                                        prop.className Css.Field
                                        prop.children [
                                            Html.div [
                                                prop.className [ Css.Control
                                                                 Css.HasIconsLeft ]
                                                prop.children [
                                                    Html.input [
                                                        prop.className (
                                                            [ Css.Input ]
                                                            |> errorProps (hasErrors "birthdate")
                                                        )
                                                        prop.placeholder "Fecha de Nacimiento"
                                                        prop.onFocus (fun e ->
                                                                        let el: HTMLInputElement = downcast e.target
                                                                        el.``type`` <- "date"
                                                                      )
                                                        prop.disabled isPosting
                                                        prop.value (
                                                            if user.birthdate = DateTime(0, 0, 0) then
                                                                ""
                                                            else
                                                                user.birthdate.ToString("yyyy-MM-dd")
                                                        )
                                                        prop.type' (
                                                            if user.birthdate = DateTime(0, 0, 0) then
                                                                "text"
                                                            else
                                                                "date"
                                                        )
                                                        (prop.onChange: (DateTime -> unit) -> _)
                                                        <| (fun birthdate -> setUser { user with birthdate = birthdate })
                                                    ]
                                                    Html.span [
                                                        prop.className [ Css.Icon; Css.IsLeft ]
                                                        prop.children [
                                                            Html.i [
                                                                prop.className [
                                                                    Css.Fa
                                                                    Icons.FaCalendar
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                    if hasErrors "birthdate" then
                                                        Html. p [
                                                            prop.className [ Css.Help; Css.IsDanger ]
                                                            prop.text errors.["birthdate"]
                                                        ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                    Html.div [
                        prop.className [ Css.Field; Css.IsHorizontal ]
                        prop.children [
                            Html.div [
                                prop.className Css.FieldBody
                                prop.children [
                                    Html.div [
                                        prop.className Css.Field
                                        prop.children [
                                            Html.div [
                                                prop.className [ Css.Control ]
                                                prop.children [
                                                    Html.textarea [
                                                        prop.className (
                                                            [ Css.Textarea ]
                                                            |> disabableProps (postingStatus = Posting)
                                                            |> errorProps (hasErrors "address")
                                                        )
                                                        prop.disabled isPosting
                                                        prop.placeholder "Dirección"
                                                        prop.value user.address
                                                        (prop.onChange: (string -> unit) -> _)
                                                        <| (fun address -> setUser { user with address = address })
                                                    ]
                                                    if hasErrors "address" then
                                                        Html. p [
                                                            prop.className [ Css.Help; Css.IsDanger ]
                                                            prop.text errors.["address"]
                                                        ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]

let renderUserHistory userHistory =
    Html.a [
        prop.className Css.PanelBlock
        prop.custom("data-aos", "fade-down")
        prop.custom("data-aos-offset", "100")
        prop.children [
            Html.span [
                prop.className Css.PanelIcon
                prop.children [
                    Html.i [
                        prop.className [ yield Css.Fa
                                         match userHistory.status with
                                         | InProgress -> yield! [ Icons.FaAddressBook; Css.HasTextWarning ]
                                         | Completed -> yield! [ Icons.FaCheck; Css.HasTextSuccess ]
                                         | Cancelled -> yield! [ Icons.FaTimes; Css.HasTextDanger ]
                                        ]
                    ]
                ]
            ]
            Html.text (
                sprintf "Historia %s: %s" (match userHistory.status with
                                           | InProgress -> "en progreso"
                                           | Completed -> "completada"
                                           | Cancelled -> "cancelada") (userHistory.date.ToString("MM/dd/yyyy"))
            )
        ]
    ]

[<ReactComponent>]
let private UserUpdate user =
    let close = React.useContext(modalContext)
    let tabs = [ "Actualizar Datos"
                 "Historias Registradas" ]
    let (selectedTab, setSelectedTab) = React.useState(tabs.[0])
    let (userHistories, setUserHistories) = React.useState([])
    let currentIndex = List.findIndex ((=) selectedTab) tabs
    let { issueSearch = issueSearch
          postingStatus = postingStatus
          setFormValue = setPackage
          formValue = package
          errors = errors } = React.useContext(formContext)
    React.useEffect(
        (fun () ->
            if postingStatus = Posted then
                let setPackage = downcast setPackage
                let package = downcast package
                setPackage { package with previousCid = package.cid  }
        ),
        [| box postingStatus |])
    React.useEffect(
        (fun () ->
         if currentIndex = 1 then
            async {
                let! response = Fetch.get(
                                    sprintf "http://192.168.1.7:3000/api/patient/history/%d" user.cid,
                                    decoder = Decode.list historyDecoder,
                                    properties = [
                                        RequestProperties.Mode RequestMode.Cors
                                    ]) |> Async.AwaitPromise
                setUserHistories response
            } |> Async.Start
         ),
        [| box selectedTab |]);
    React.fragment(
        [
            Html.div [
                prop.className Css.ModalContent
                prop.children [
                    Html.nav [
                        prop.className [ Css.Panel; Css.IsDark; Css.Card; Css.M2 ]
                        prop.children [
                            Html.p [
                                prop.className Css.PanelHeading
                                prop.text "Información de Paciente"
                            ]
                            if postingStatus = Posted || Map.containsKey "posting" errors then
                                Html.div [
                                    prop.className [ Css.PanelBlock ]
                                    prop.children [
                                        Html.article [
                                            prop.className [ Css.Message
                                                             "fmessage"
                                                             if postingStatus = Posted then
                                                                Css.IsSuccess
                                                             else
                                                                Css.IsDanger
                                                            ]
                                            prop.children [
                                                Html.div [
                                                    prop.className Css.MessageBody
                                                    prop.text (
                                                        if postingStatus = Posted then
                                                            "Los datos del usuario han sido actualizados"
                                                        else
                                                            sprintf "No se pudo completar la solicitud: %s" errors.["posting"]
                                                    )
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            Html.div [
                                prop.className [ Css.PanelTabs; Css.Mt3 ]
                                prop.children [
                                    for tab in tabs do
                                    Html.a [
                                        prop.text tab
                                        prop.disabled <| (postingStatus = Posting)
                                        if tab = selectedTab then prop.className Css.IsActive
                                        prop.onClick (fun _ -> setSelectedTab tab)
                                    ]
                                ]
                            ]
                            match currentIndex with
                            | 0 ->
                                Html.div [
                                    prop.className [
                                        Css.PanelBlock
                                        Css.IsJustifyContentCenter
                                    ]
                                    prop.children [
                                        Html.div [
                                            prop.className [
                                                Css.Container
                                                Css.IsFullwidth
                                            ]
                                            prop.children [ UserForm() ]
                                        ]
                                    ]
                                ]
                            | _ ->
                                React.fragment [
                                    for userHistory in userHistories do
                                    renderUserHistory userHistory
                                ]
                            Html.div [
                                prop.className Css.PanelBlock
                                prop.children [
                                    Html.button [
                                        prop.className [ Css.Button
                                                         Css.IsFullwidth
                                                         "fbutton"
                                                         if postingStatus = Posting then
                                                            Css.IsLoading ]
                                        prop.text (match currentIndex with
                                                   | 0 -> "Actualizar"
                                                   | _ -> "Registrar nueva historia")
                                        prop.disabled <| (postingStatus = Posting)
                                        prop.onClick (fun _ -> if currentIndex = 0 then issueSearch())
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
            Html.button [
                prop.className [ Css.ModalClose; Css.IsLarge ]
                prop.onClick (fun _ -> if postingStatus <> Posting then close())
            ]
        ]
    )

[<ReactComponent>]
let rec PatientIndex() =
    StreamIndex [
        StreamIndexProps.title (
            Html.p [
                prop.className Css.Title
                prop.text "Manejo de Pacientes"
            ]
        )
        StreamIndexProps.subtitle (
            Html.div [ prop.className Css.Content; prop.children [
                Html.p [
                    prop.className [ Css.Subtitle; Css.Mt1 ]
                    prop.text """En esta vista se realiza el manejo
                            de los pacientes de la fundación.
                            Para el rol actual, se permiten las siguientes actividades"""
                ]
                Html.ul [
                    prop.children [
                        for act in [ "Registrar pacientes"
                                     "Actualizar pacientes"
                                     "Consultar pacientes" ] do
                        Html.li act
                    ]
                ]
            ]]
        )
        StreamIndexProps.storeName
        <| nameof PatientIndex
        StreamIndexProps.packageEncoder
        <| Encode.Auto.generateEncoder<UserRecord>()
        StreamIndexProps.packageDecoder
        <| Decode.oneOf [
            userRecordDecoder
            Decode.Auto.generateDecoderCached()
        ]
        StreamIndexProps.queryEncoder queryEncoder
        StreamIndexProps.wsUrl "ws://192.168.1.7:3000/api/patient/stream"
        StreamIndexProps.packageDescriptor (fun desc pat ->
                match desc with
                | Birthdate -> Html.td pat.age
                | Fullname -> Html.td pat.fullname
                | Cid -> Html.td pat.cid
                | Address -> Html.td pat.address
                | _ -> Html.none
            )
        StreamIndexProps.descriptors [
            Cid, "Cédula de Identidad"
            Fullname, "Nombre y Apellido"
            Address, "Dirección"
            Birthdate, "Edad"
            All, "Todos"
        ]
        StreamIndexProps.onAddComponent (
            ModalForm [
                ModalFormProps.encoder userDataEncoder
                ModalFormProps.initialValue
                <| { cid = 0; names = ""; lastnames = ""; birthdate = DateTime(0, 0, 0); address = ""; previousCid = 0 }
                ModalFormProps.url "http://192.168.1.7:3000/api/patient/create"
                ModalFormProps.rules [
                    ModalFormProps.rule "cid"
                    <| emptyField (fun user -> if user.cid = 0 then "" else user.cid.ToString()) "La cédula de identidad no puede estar vacía"
                    ModalFormProps.rule "birthdate"
                    <| emptyField (fun user ->
                                   if user.birthdate = DateTime(0, 0, 0) then ""
                                   else user.birthdate.ToString()) "La fecha de nacimiento no puede estar vacía"
                    ModalFormProps.rule "address"
                    <| (
                        emptyField (fun user -> user.address) "La dirección no puede estar vacía"
                        >=>  maxLength (fun user -> user.address) 255 "La dirección debe tener más de 255 caracteres"
                    )
                    ModalFormProps.rule "names"
                    <| (
                        emptyField (fun user -> user.names) "Los nombres no pueden estar vacíos"
                        >=>  maxLength (fun user -> user.names) 50 "Los nombres no pueden tener más de 50 caracteres"
                    )
                    ModalFormProps.rule "lastnames"
                    <| (
                        emptyField (fun user -> user.lastnames) "Los apellidos no pueden estar vacíos"
                        >=>  maxLength (fun user -> user.lastnames) 65 "Los apellidos no pueden tener más de 65 caracteres"
                    )
                ]
                ModalFormProps.renderer
                << ModalDecorator "Nuevo Paciente" "Agregar Paciente"
                <| UserForm()
            ]
        )
        StreamIndexProps.clickModalComponent (fun user ->
            let userRecord = {
                names = user.fullname.Split(",").[0]
                lastnames = user.fullname.Split(",").[1].Trim()
                cid = user.cid
                address = user.address
                birthdate = user.birthdate
                previousCid = user.cid
            }
            ModalForm [
                ModalFormProps.encoder (fun userPkg ->
                                        Encode.object [
                                            "previous_cid", Encode.int userPkg.previousCid
                                            "body", userDataEncoder userPkg
                                        ])
                ModalFormProps.initialValue userRecord
                ModalFormProps.url "http://192.168.1.7:3000/api/patient/update"
                ModalFormProps.httpMethod HttpMethod.PUT
                ModalFormProps.rules [
                    ModalFormProps.rule "cid"
                    <| emptyField (fun user -> if user.cid = 0 then "" else user.cid.ToString()) "La cédula de identidad no puede estar vacía"
                    ModalFormProps.rule "birthdate"
                    <| emptyField (fun user ->
                    if user.birthdate = DateTime(0, 0, 0) then ""
                    else user.birthdate.ToString()) "La fecha de nacimiento no puede estar vacía"
                    ModalFormProps.rule "address"
                    <| (
                        emptyField (fun user -> user.address) "La dirección no puede estar vacía"
                        >=>  maxLength (fun user -> user.address) 255 "La dirección debe tener más de 255 caracteres"
                    )
                    ModalFormProps.rule "names"
                    <| (
                        emptyField (fun user -> user.names) "Los nombres no pueden estar vacíos"
                        >=>  maxLength (fun user -> user.names) 50 "Los nombres no pueden tener más de 50 caracteres"
                    )
                    ModalFormProps.rule "lastnames"
                    <| (
                        emptyField (fun user -> user.lastnames) "Los apellidos no pueden estar vacíos"
                        >=>  maxLength (fun user -> user.lastnames) 65 "Los apellidos no pueden tener más de 65 caracteres"
                    )
                ]
                ModalFormProps.renderer <| UserUpdate userRecord
            ]
        )
    ]
