module LazyLoadChildren

open Fable
open Feliz

[<ReactComponent>]
let LazyLoadChildren children =
    React.fragment(children)
