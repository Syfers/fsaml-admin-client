module Layout
open Feliz
open Fable.Core
open Extensions
open Fable.Core.JsInterop
open Fable.React

type MenuItem = {
    name: string
    link: string
    icon: string
    selected: bool
}

type LayoutState =
    | Expanded
    | Collapsed

let oneColumnLayoutContext =
    React.createContext(
        name="oneColumnLayoutContext",
        defaultValue=fun (_: LayoutState) -> ()
    );

module React =
    let useMediaQuery: {| query: string |} -> bool = importMember "react-responsive"

let private Branding() =
    let icon = importDefault "../logo_white.svg"
    Html.div [
            prop.className Css.NavbarBrand
            prop.children [
                Html.a [
                    prop.id "fbranding"
                    prop.className Css.NavbarItem
                    prop.href "#"
                    prop.children [
                        Html.img [
                            prop.src icon
                        ]
                    ]
                ]
                Html.div [
                    prop.className Css.NavbarBurger
                    prop.custom("data-target", "navbar-body")
                    prop.children [
                        Html.span []
                        Html.span []
                        Html.span []
                    ]
                ]
            ]
        ]

[<ReactComponent>]
let OneColumnLayout (children: ReactElement list) =
    let (layoutState, setLayoutState) = React.useState(Collapsed)
    let matchesNarrow = React.useMediaQuery({| query= "(max-width: 768px)" |})
    React.contextProvider(oneColumnLayoutContext, setLayoutState,
                          if not matchesNarrow then
                                Html.div [
                                    prop.className [
                                        Css.Card
                                        Css.IsHiddenMobile
                                        Css.IsWhite
                                    ]
                                    prop.children [
                                        Html.div [
                                            prop.className [ yield Css.P6
                                                             yield Css.Hero
                                                             if layoutState = Expanded then
                                                                yield Css.IsFullheight ]
                                            prop.children children
                                        ]
                                    ]
                                ]
                          else
                                Html.div [
                                    prop.className [
                                        Css.IsHiddenTablet
                                        Css.Hero
                                        if layoutState = Expanded then
                                            Css.IsFullheight
                                        ]
                                    prop.children children
                                ]
    )


[<ReactComponent>]
let MainLayout menuItems  children =
    Html.div[
            prop.children [
                Html.nav [
                    prop.className [ Css.Navbar; Css.IsMobile; Css.IsDark; Css.IsFixedTop ]
                    (prop.children: ReactElement list -> IReactProperty)
                    <| Branding() :: [
                        Html.div [
                            prop.className Css.NavbarMenu
                            prop.id "navbar-body"
                            prop.children [
                                Html.div [
                                    prop.className Css.NavbarStart
                                    prop.children [
                                        for menuItem in menuItems do
                                        Html.a [
                                            prop.className [ Css.NavbarItem; Css.IsHoverable; if menuItem.selected then "fselected" ]
                                            prop.href menuItem.link
                                            prop.children [
                                                Html.span [
                                                    prop.className Css.IconText
                                                    prop.children [
                                                        Html.span [
                                                            prop.className Css.Icon
                                                            prop.children [
                                                                Html.i [ prop.className [ Css.Fa; menuItem.icon ] ]
                                                            ]
                                                        ]
                                                        Html.p [
                                                            prop.text menuItem.name
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
                Html.div [
                    prop.className [ Css.Section; Css.Container; Css.Mt5; ]
                    (prop.children: ReactElement list -> IReactProperty) children
                ]
            ]
        ]
