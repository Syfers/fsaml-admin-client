[<AutoOpen>]
module Extensions

open System
open Fable.Core
open Fable.Core.JsInterop
open Zanaptak.TypedCssClasses

[<RequireQualifiedAccess>]
module StaticFile =

    /// Function that imports a static file by it's relative path.
    let inline import (path: string) : string = importDefault<string> path

[<RequireQualifiedAccess>]
module Config =
    /// Returns the value of a configured variable using its key.
    /// Retursn empty string when the value does not exist
    [<Emit("process.env[$0] ? process.env[$0] : ''")>]
    let variable (_: string) : string = jsNative

    /// Tries to find the value of the configured variable if it is defined or returns a given default value otherwise.
    let variableOrDefault (key: string) (defaultValue: string) =
        let foundValue = variable key
        if String.IsNullOrWhiteSpace foundValue
        then defaultValue
        else foundValue

// Stylesheet API
// let private stylehsheet = Stylesheet.load "./fancy.css"
// stylesheet.["fancy-class"] which returns a string
module Stylesheet =

    type IStylesheet =
        [<Emit "$0[$1]">]
        abstract Item : className:string -> string

    /// Loads a CSS module and makes the classes within available
    let inline load (path: string) = importDefault<IStylesheet> path

type Css = CssClasses<"https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css", Naming.PascalCase>
type Icons = CssClasses<"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css", Naming.PascalCase>

// Data getter and setter for generic contexts
// [In provider] React.contextProvider( ..., DataProvider(value, valueSetter), ...)
// [In consumer] let context = React.useContext(...); let parentData = context.Get(); let parentDataSetter = context.Setter();
type DataProvider(value, setter) =
    let value = value :> obj
    let setter = setter :> obj

    member _.Get() = downcast value
    member _.Setter() = downcast setter
