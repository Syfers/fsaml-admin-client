module StreamIndex

open Fable
open Feliz
open Thoth.Json
open FSharpPlus.Operators
open StreamProtocol
open Fable.React
open FSharpPlus.Data
open System

type StreamIndexInputOptions<'pkg, 'queryType> = {
    _title: ReactElement
    _subtitle: ReactElement
    _packageDecoder: Option<Decoder<'pkg>>
    _queryEncoder: Option<Query<'queryType> -> int -> JsonValue>
    _packageDescriptor: Option<'queryType -> 'pkg -> ReactElement>
    _descriptorToDisplay: Option<'queryType -> string>
    _displayToDescriptor: Option<string -> 'queryType>
    _descriptorsNames: string list
    _wsUrl: string
}

type private StreamIndexOptions<'pkg, 'queryType> = {
    title: ReactElement
    subtitle: ReactElement
    packageDecoder: Decoder<'pkg>
    queryEncoder: Query<'queryType> -> int -> JsonValue
    packageDescriptor: 'queryType -> 'pkg -> ReactElement
    descriptorToDisplay: 'queryType -> string
    displayToDescriptor: string -> 'queryType
    descriptorsNames: string list
    wsUrl: string
}

module StreamIndexProps =
    let title title =
        State.modify <| fun options -> { options with _title = title }
    let subtitle subtitle =
        State.modify <| fun options -> { options with _subtitle = subtitle }
    let packageDecoder decoder: State<StreamIndexInputOptions<_,_>,_> =
        State.modify <| fun options ->
            { options with _packageDecoder = Some decoder }
    let queryEncoder encoder: State<StreamIndexInputOptions<_,_>,_> =
        State.modify <| fun options ->
            { options with _queryEncoder = Some encoder }
    let packageDescriptor descriptor =
        State.modify <| fun options -> { options with _packageDescriptor = Some descriptor }
    let descriptors descriptors =
        let map = Map.ofList descriptors
        let inverseMap = Map.ofList <| List.map (fun (a, b) -> b, a) descriptors
        State.modify <| fun options ->
            { options with
                _descriptorToDisplay = Some <| fun k -> map.[k]
                _displayToDescriptor = Some <| fun k -> inverseMap.[k]
                _descriptorsNames = List.map snd descriptors
            }
    let wsUrl url : State<StreamIndexInputOptions<_,_>,_> =
        State.modify <| fun options -> { options with _wsUrl = url }
    let build props options =
        List.fold (flip State.exec) options props

let private packageDecoder decoder =
    Decode.oneOf [
        Decode.object
            (fun get ->
                {
                    queryId = get.Required.Field "query_id" Decode.int
                    pages = get.Required.Field "total_pages" Decode.int
                }
             )
        |> Decode.map (Choice2Of2)
        decoder
        |> Decode.map (Choice1Of2)
    ]

let SearchForm (onTextChanged: string -> unit) (onTypeChanged: string -> unit) options =
    Html.div [
        Html.form [
            prop.className [ Css.IsHiddenMobile ]
            prop.children [
                Html.div [
                    prop.className [ Css.Field; Css.IsHorizontal; Css.HasAddons; Css.HasAddonsCentered ]
                    prop.children [
                        Html.p [
                            prop.className Css.Control
                            prop.children [
                                Html.span [
                                    prop.className [ Css.Select; Css.IsMedium ]
                                    prop.children [
                                        Html.select [
                                            prop.onChange onTypeChanged
                                            prop.children [
                                                    for item in options do
                                                    (Html.option: string -> ReactElement) item
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                        Html.p [
                            prop.className [ Css.Control; Css.IsExpanded ]
                            prop.children [
                                Html.input [
                                    prop.className [ Css.Input; Css.IsMedium ]
                                    prop.type' "text"
                                    prop.onChange(onTextChanged)
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
        Html.form [
            prop.className [ Css.IsHiddenTablet ]
            prop.children [
                Html.div [
                    prop.className [ Css.Field; Css.IsHorizontal ]
                    prop.children [
                        Html.div [
                            prop.className [ Css.FieldLabel; Css.IsNormal ]
                            prop.children [
                                Html.label [
                                    prop.className Css.Label
                                    prop.text "Campo de búsqueda"
                                ]
                            ]
                        ]
                        Html.div [
                            prop.className Css.FieldBody
                            prop.children [
                                Html.div [
                                    prop.className Css.Field
                                    prop.children [
                                        Html.p [
                                            prop.className [ Css.Control; Css.IsExpanded; Css.IsMobile ]
                                            prop.children [
                                                Html.span [
                                                    prop.className Css.Select
                                                    prop.children [
                                                        Html.select [
                                                            prop.onChange onTypeChanged
                                                            prop.children [
                                                                    for item in options do
                                                                    Html.option item
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
                Html.div [
                    prop.className [ Css.Field; Css.IsHorizontal ]
                    prop.children [
                        Html.div [
                            prop.className [ Css.FieldLabel; Css.IsNormal ]
                            prop.children [
                                Html.label [
                                    prop.className Css.Label
                                    prop.text "Texto de búsqueda"
                                ]
                            ]
                        ]
                        Html.div [
                            prop.className Css.FieldBody
                            prop.children [
                                Html.div [
                                    prop.className Css.Field
                                    prop.children [
                                        Html.p [
                                            prop.className [ Css.Control; Css.IsExpanded; Css.HasIconsLeft ]
                                            prop.children [
                                                Html.input [
                                                    prop.onChange onTextChanged
                                                    prop.className Css.Input
                                                ]
                                                Html.span [
                                                    prop.className [ Css.Icon; Css.IsLeft ]
                                                    prop.children [
                                                        Html.i [ prop.className [ Css.Fa; Icons.FaSearch ] ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]

[<ReactComponent>]
let private StreamIndexComponent propOptions =
    let protocolContext = React.useContext(wsProtocolContext)
    let protocolState  = ( downcast protocolContext.protocolState)
    let searchState = protocolState.searchState
    let queryPages = protocolState.currentPages
    let query = protocolState.query
    let setQuery = downcast protocolContext.setQuery
    let dispatchLayoutChanged = React.useContext(Layout.oneColumnLayoutContext)
    React.useEffect(
        (fun () ->
            setQuery query),
        [| box query |]
        )
    React.useEffect(
        ( fun () ->
            if searchState.isResult then
                do dispatchLayoutChanged Layout.Expanded
            else
                do dispatchLayoutChanged Layout.Collapsed
          )
        ,[| box searchState |])
    Html.div [
        propOptions.title
        propOptions.subtitle
        SearchForm
        <| (fun newSearch ->
            Console.WriteLine(newSearch)
            setQuery { query with search = newSearch; pageSearch = 1 })
        <| (propOptions.displayToDescriptor
            >> fun newType -> setQuery { query with ``type`` = newType })
        <| Seq.ofList(propOptions.descriptorsNames)
        Html.section [
            prop.className [ Css.Container; Css.My6 ]
            prop.children [
                match searchState with
                | NoResult msg ->
                    Html.article [
                        prop.className [ Css.Message ]
                        prop.children [
                            Html.div [
                                prop.className Css.MessageHeader
                                prop.children [
                                    Html.p "Sin Resultados"
                                ]
                            ]
                            Html.div [
                                prop.className Css.MessageBody
                                prop.text msg
                            ]
                        ]
                    ]
                | Result queryResult ->
                    Html.nav [
                        prop.className [ Css.Pagination; Css.IsCentered ]
                        prop.role "navigation"
                        prop.ariaLabel "pagination"
                        prop.children [
                            yield Html.a [
                                prop.className Css.PaginationPrevious
                                prop.text "Anterior"
                                prop.onClick (fun _ ->
                                              if query.pageSearch > 1 then
                                                setQuery { query with pageSearch = query.pageSearch - 1 })
                            ]
                            yield Html.a [
                                prop.className Css.PaginationNext
                                prop.text "Siguiente"
                                prop.onClick (fun _ ->
                                              if query.pageSearch < queryPages then
                                                setQuery { query with pageSearch = query.pageSearch + 1 })
                            ]
                            yield Html.ul [
                                prop.className Css.PaginationList
                                prop.children [
                                    if queryPages <= 10 then
                                        for index in 1..queryPages do
                                        yield Html.li [
                                                Html.a [
                                                    prop.className (
                                                        if query.pageSearch = index then
                                                            [ Css.PaginationLink; Css.IsCurrent ]
                                                        else
                                                            [ Css.PaginationLink ]
                                                    )
                                                    prop.onClick (fun _ -> setQuery { query with pageSearch = index })
                                                    prop.text index
                                                ]
                                        ]
                                    else
                                        for index in 1..3 do
                                            yield (Html.li: ReactElement list -> ReactElement) << List.singleton
                                            <| Html.a [
                                                prop.className (
                                                    if query.pageSearch = index then
                                                        [ Css.PaginationLink; Css.IsCurrent ]
                                                    else
                                                        [ Css.PaginationLink ]
                                                )
                                                prop.onClick (fun e ->
                                                    e.preventDefault()
                                                    setQuery { query with pageSearch = index })
                                                prop.text index
                                            ]
                                        yield (Html.li: ReactElement list -> ReactElement) << List.singleton
                                        <| Html.span [
                                            prop.className Css.PaginationEllipsis
                                            prop.text "..."
                                        ]
                                        if not <| List.contains query.pageSearch [ 1; 2; 3; queryPages; queryPages - 1; queryPages - 2 ] then
                                            yield! [
                                                (Html.li: ReactElement list -> ReactElement) << List.singleton
                                                <| Html.a [
                                                    prop.className [ Css.PaginationLink; Css.IsCurrent ]
                                                    prop.text query.pageSearch
                                                ]
                                                (Html.li: ReactElement list -> ReactElement) << List.singleton
                                                <| Html.span [
                                                    prop.className Css.PaginationEllipsis
                                                    prop.text "..."
                                                ]
                                            ]
                                        for index in 0..2 do
                                            yield (Html.li: ReactElement list -> ReactElement) << List.singleton
                                            <| Html.a [
                                                prop.className (
                                                    if query.pageSearch = queryPages - ( 2 - index ) then
                                                        [ Css.PaginationLink; Css.IsCurrent ]
                                                    else
                                                        [ Css.PaginationLink ]
                                                )
                                                prop.onClick (fun _ ->
                                                    setQuery { query with pageSearch = queryPages - ( 2 - index ) })
                                                prop.text (queryPages - ( 2 - index ))
                                            ]

                                ]
                            ]
                        ]
                    ]
                    Html.table [
                        prop.className [ Css.Table; Css.IsFullwidth; Css.IsHoverable; Css.IsStriped ]
                        prop.children [
                            Html.thead [
                                prop.children [
                                    Html.tr [
                                        for desc in propOptions.descriptorsNames do
                                            if desc <> "Todos" then
                                                Html.th desc
                                    ]
                                ]
                            ]
                            Html.tbody [
                                for package in queryResult do
                                Html.tr [
                                    prop.custom("data-aos", "fade-right")
                                    prop.custom("data-aos-offset", "50")
                                    prop.children [
                                        let props = propOptions
                                        for desc in props.descriptorsNames do
                                            if desc <> "Todos" then
                                                props.packageDescriptor
                                                <| (props.displayToDescriptor desc)
                                                <| package
                                    ]
                                ]
                            ]
                        ]
                    ]
            ]
        ]
    ]

[<ReactComponent>]
let StreamIndex props =
    let propOptions =
        StreamIndexProps.build
        <| props
        <| {
        _title =
            Html.p [
                prop.className Css.Title
                prop.text "StreamIndex"
            ]
        _subtitle =
            Html.p [
                prop.className Css.Title
                prop.text "StreamIndex Description"
            ]
        _packageDecoder = None
        _queryEncoder = None
        _packageDescriptor = None
        _descriptorToDisplay = None
        _displayToDescriptor = None
        _descriptorsNames = [  ]
        _wsUrl = null
    }
    let options = {
            title = propOptions._title
            subtitle = propOptions._subtitle
            packageDecoder = Option.get <| propOptions._packageDecoder
            packageDescriptor = Option.get <| propOptions._packageDescriptor
            queryEncoder = Option.get <| propOptions._queryEncoder
            descriptorToDisplay = Option.get <| propOptions._descriptorToDisplay
            displayToDescriptor = Option.get <| propOptions._displayToDescriptor
            descriptorsNames = propOptions._descriptorsNames
            wsUrl = propOptions._wsUrl
        }
    WebSocketProvider options.wsUrl [
        StreamProtocol [
            StreamProtocolProps.url options.wsUrl
            StreamProtocolProps.descriptorToDisplay options.descriptorToDisplay
            StreamProtocolProps.initialDescriptor
                <| options.displayToDescriptor(options.descriptorsNames.Head)
            StreamProtocolProps.packageDecoder
                <| options.packageDecoder
            StreamProtocolProps.queryEncoder options.queryEncoder
            StreamProtocolProps.descriptorToDisplay options.descriptorToDisplay
            StreamProtocolProps.children [ StreamIndexComponent options ]
        ]
    ]
